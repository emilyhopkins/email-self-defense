# This source code is released in public domain.

# Author Tomas Stary (2016) <tomichec@gmail.com> 
# Thanks for helpfull discussion to Thérèse Godefroy

# this Makefile.gen is used to generate the translated html files from the
# PO templates

# see Makefile for generation of translation files (po, pot) and
# extraction of translated strings from existing translation

# usage: make -f Makefile.gen

# IMPORTANT
# 1. Make sure your esd-LANG.po is encoded in unix format (end
#    of line \n, not DOS \n\r), otherwise po4a-translate complains
#    about strange lines.
# 2. Before generating a page for publication, make sure the PO has no
#    untranslated or fuzzy strings, otherwise the translation will be
#    interspersed with English strings.


# In LANG add the code(s) of the language(s) you intend to translate
LANG=pt-br

# flags for the po4a-getextize
PO4A_FLAGS=-M utf-8 -f xhtml --keep 30 -o porefs=none \
           -o 'untranslated=<script>' \
           -o 'attributes=<meta>content'

# list of source files for the translation
SRC=confirmation.html\
	index.html\
	infographic.html\
	mac.html\
	next_steps.html\
	windows.html\
	workshops.html

# default target
all: $(foreach l,$(LANG),$(foreach f,$(SRC),$l/$f))


## RECIPES for the individual pages in each language

# Add yours if you migrate to PO, keeping the alphabetical order.

# NOTES
# 1. You can use another recipe as a model, but be careful with global image
#    localization; make sure that _all_ images are in the static/LANG/img
#    subdirectory.
# 2. The following modifications are essential in right to left languages:
#	sed -i 's|<html|<html dir="rtl"|' $@
#	sed -i '/\/css\/main.css/a\
#	<link rel="stylesheet" href="//static.fsf.org/nosvn/enc-dev0/css/main.rtl.css" />' $@
#    See Persian recipe as an example.

# cs - Czech
# sed -i 's|<li><a href="/en">English - v4.0</a></li>|<li><a class="current" href="/cs">Čeština - v4.0</a></li>\
# 				<li><a href="/en">English - v4.0</a></li>|' $@
cs/%.html: esd-cs.po $(foreach f,$(SRC),en/$f)
	-po4a-translate $(PO4A_FLAGS) -m en/$*.html -p $< -l $@
	sed -i 's|img/en/|img/cs/|g' $@

# es - Spanish
es/%.html: esd-es.po $(foreach f,$(SRC),en/$f)
	-po4a-translate $(PO4A_FLAGS) -m en/$*.html -p $< -l $@
	sed -i '/\/css\/main.css/a\
	<link rel="stylesheet" href="//static.fsf.org/nosvn/enc-dev0/css/main.es.css" />' $@

# fa - Persian
fa/%.html: esd-fa.po $(foreach f,$(SRC),en/$f)
	-po4a-translate $(PO4A_FLAGS) -m en/$*.html -p $< -l $@
	sed -i 's|<html|<html dir="rtl"|' $@
	sed -i '/\/css\/main.css/a\
	<link rel="stylesheet" href="//static.fsf.org/nosvn/enc-dev0/css/main.rtl.css" />' $@

# ja - Japanese
ja/%.html: esd-ja.po $(foreach f,$(SRC),en/$f)
	-po4a-translate $(PO4A_FLAGS) -m en/$*.html -p $< -l $@

# pt-br - Brazilian Portuguese
pt-br/%.html: esd-pt-br.po $(foreach f,$(SRC),en/$f)
	-po4a-translate $(PO4A_FLAGS) -m en/$*.html -p $< -l $@
	sed -i '/\/donate.png" \/><\/a><\/p>/a\
	\n<div class="fsf-emphasis"><p>Conheça também a <a\nhref="http://www.fsfla.org/ikiwiki/index.pt.html">FSFLA</a>, organização\nirmã da FSF na América Latina.</p></div>' $@

# sq - Albanian
sq/%.html: esd-sq.po $(foreach f,$(SRC),en/$f)
	-po4a-translate $(PO4A_FLAGS) -m en/$*.html -p $< -l $@

# zh-han - Chinese (Han)
zh-hans/%.html: esd-zh-hans.po $(foreach f,$(SRC),en/$f)
	-po4a-translate $(PO4A_FLAGS) --keep 0 -m en/$*.html -p $< -l $@

