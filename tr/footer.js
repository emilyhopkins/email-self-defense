 document.write(`
<!-- ~~~~~~~~~ Footer  ~~~~~~~~~ -->
		<footer class="row" id="footer">
			<div>
				<div id="copyright">
<h4><a href="https://u.fsf.org/ys"><img alt="Özgür Yazılım Vakfı" src="//static.fsf.org/nosvn/enc-dev0/img/fsf-logo.png"></a></h4>
<p>Copyright &copy; 2014-2016 <a href="https://u.fsf.org/ys">Özgür Yazılım Vakfı</a>, Inc. <a href="https://my.fsf.org/donate/privacypolicy.html">Gizlilik Politikası</a>. Lütfen <a href="https://u.fsf.org/yr">üyemiz olarak</a> bize destek olun </p>

<!--<p><em>Sürüm 4.0. Türkçe'ye çeviren T. E. KALAYCI.</em></p>-->

<p>Bu sayfadaki resimler <a href="https://creativecommons.org/licenses/by/4.0/deed.tr">Creative Commons Alıntı 4.0 lisansı (veya sonraki sürümü)</a> ile, geri kalan herşey <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.tr">Creative Commons Atıf-LisansDevam 4.0 lisansı (veya sonraki sürümü)</a> ile lisanslanmıştır. Andrew Engelbrecht <sudoman@ninthfloor.org> ve Josh Drake <zamnedix@gnu.org> tarafında geliştirilen ve GNU Affero Genel Kâmu Lisansı altında yayınlanan <a href="http://agpl.fsf.org/emailselfdefense.fsf.org/edward/CURRENT/edward.tar.gz">Edward yanıt botunun kaynak kodunu</a> indirebilirsiniz.  <a href="http://www.gnu.org/licenses/license-list.html#OtherLicenses">Neden bu lisanslar?</a></p>

<p>Rehberde ve infografikte kullanılan yazı tipleri: <a href="https://www.google.com/fonts/specimen/Dosis">Dosis</a> (Pablo Impallari), <a href="http://www.google.com/fonts/specimen/Signika">Signika</a> (Anna Giedry&#347;), <a href="http://www.google.com/fonts/specimen/Archivo+Narrow">Archivo Narrow</a> (Omnibus-Type), <a href="https://libreplanet.org/wiki/GPG_guide/Graphics_Howto#Pitfalls">PXL-2000</a> (Florian Cramer).</p>

<p>Fontları, resim kaynak dosyalarını ve Edward'ın mesajlarının metinlerini de içeren bu rehberin <a href="emailselfdefense_source.zip">kaynak kodlarını</a> indirin.</p>

<p>Bu site, Weblabels standartı olan <a href="https://www.fsf.org/campaigns/freejs">özgür JavaScript</a> etiketlendirmesini kullanıyor. JavaScript
<a href="//weblabels.fsf.org/emailselfdefense.fsf.org/" rel="jslicense">kaynak kodu ve lisans bilgisini</a> inceleyin.</p>
				</div><!-- /#copyright -->
				<p class="credits">
					İnfografik ve rehber tasarımını yapan: <a rel="external" href="http://jplusplus.org"><strong>Journalism++</strong> <img src="//static.fsf.org/nosvn/enc-dev0/img/jplusplus.png" alt="Journalism++" /></a>
				</p><!-- /.credits -->
			</div>
		</footer><!-- End #footer -->`)
